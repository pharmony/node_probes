name 'node_probes'
maintainer 'Pharmony SA'
maintainer_email 'infra@pharmony.lu'
license 'All Rights Reserved'
description "Installs/Configures Pharmony's probes"
version '0.5.0'
chef_version '>= 14.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/pharmony/node_probes/-/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/pharmony/node_probes'
