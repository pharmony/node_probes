# node_probes

Pharmony has developed probe scripts which are watching node's items and report to Slack any failures.
The goal is to be immeditaly informed of failures and not spending time to track them when it's too late.

## Probe list

### Flannel probe

The Flannel probe is a Bash script which watch the Flannel Kubernetes CNI driver network interface and sends message to Slack when the interface is missing.

Typically this happen when TincVPN restarts. Loosing this network interface isolate the Kubernetes node from any activities.

### Tinc check probe

The TinVPC check probe pings the TincVPN network members and report any failures to Slack.

## Attributes:

```ruby
# Name of the region where the node is (example: east)
default['node_probes'] = {
  region_name: 'east',
  slack_webhook_url: 'https://hooks.slack.com/services/T0...',
  slack_channel: 'servers'
}
```
