# Name of the region where the node is (example: east)
default['node_probes']['region_name'] = nil

# Slack Webhook where messages are posted when the flannel network dev is missing
default['node_probes']['slack_webhook_url'] = nil
default['node_probes']['slack_channel'] = nil

default['node_probes']['flannel_enable'] = false
