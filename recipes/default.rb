#
# Cookbook:: node_probes
# Recipe:: default
#
# Copyright:: 2020, Pharmony SA, All Rights Reserved.

# This script sets an environment variable containing the name of the region
# where this node is.
template '/etc/profile.d/cluster-region.sh' do
  source 'cluster-region.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables(cluster_region_name: node['node_probes']['region_name'])
end

# Installs the flannel-probe
template '/usr/local/bin/flannel-probe' do
  source 'flannel-probe.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables(slack_webhook_url: node['node_probes']['slack_webhook_url'],
            slack_channel: node['node_probes']['slack_channel'])
  only_if { node['node_probes']['flannel_enable'] }
end

# Install the TincVPN probe
template '/usr/local/bin/tinc-check' do
  source 'tinc-check.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables(slack_webhook_url: node['node_probes']['slack_webhook_url'],
            slack_channel: node['node_probes']['slack_channel'])
end
# And its service file
template '/etc/systemd/system/tinc-check.service' do
  source 'tinc-check.service.erb'
  owner 'root'
  group 'root'
  mode '0755'
end

# Installs the Bash script to send Slack messages
template '/usr/local/bin/notify_slack.sh' do
  source 'notify_slack.erb'
  owner 'root'
  group 'root'
  mode '0755'
end

# Adds a cron entry to execute the flannel probe each minutes
cron 'node_probes' do
  command '/usr/local/bin/flannel-probe'
  only_if { node['node_probes']['flannel_enable'] }
end

# Starts the tinc-check script
service 'tinc-check' do
  action [:enable, :start]
end
