# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]



## [0.5.0] - 2021-03-22
### Changed
- Prevents creating the Flannel cron task when disabled

## [0.4.0] - 2021-03-22
### Added
- Allow disabling flannel proble and disables it by default

## [0.3.0] - 2020-12-15
### Changed
- Updates the tinc probe failure message in order to include the hostfile name in the error message (which is the hostname we're trying to ping)

## [0.2.0] - 2020-12-15
### Added
- TincVPN Check probe

## [0.1.0] - 2020-12-03
### Added
- Flannel probe

[Unreleased]: https://gitlab.com/pharmony/flannel_probe/-/compare/v0.5.0...master
[0.3.0]: https://gitlab.com/pharmony/flannel_probe/-/compare/v0.4.0...v0.5.0
[0.3.0]: https://gitlab.com/pharmony/flannel_probe/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/pharmony/flannel_probe/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/pharmony/flannel_probe/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/pharmony/flannel_probe/-/tags/v0.1.0
